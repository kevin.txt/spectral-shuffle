// C++ standard library
#include <random>
#include <utility>
#include <vector>

// <random>
using std::random_device;
using std::uniform_int_distribution;
// <utility>
using std::swap;
// <vector>
using std::vector;

template <typename T>
void Full_Alter(vector<T> &a) {
    // Initialize generator.
    random_device generator;
    // Fisher–Yates shuffle:
    // To shuffle an array of n elements (indices 0 to n-1):
    // For i from n-1 to 1:
    for (unsigned i = a.size() - 1; i > 0; --i) {
        // Initialize distribution.
        uniform_int_distribution<int> distribution(0,i);
        // Swap a[i] and a[j] s.t. j is a random integer in [0,i].
        swap(a[i], a[distribution(generator)]);
    }
}
