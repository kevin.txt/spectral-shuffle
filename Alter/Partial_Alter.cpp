// C++ standard library
#include <iostream>
#include <vector>

// <iostream>
using std::cout;
using std::endl;
// <vector>
using std::vector;

template <typename T>
void Partial_Alter(vector<T> &a) {
    cout << "Partial Alter not written. No Alter will be performed." << endl;
    // Prevent compiler from issuing an unused variable warning.
    a.shrink_to_fit();
    return;
}
