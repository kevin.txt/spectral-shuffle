// C++ standard library
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
// Alters
#include "Alter/Full_Alter.cpp"
#include "Alter/Partial_Alter.cpp"
// Mapping struct and Spectral Map
#include "Map/mapping.hpp"
#include "Map/Spectral_Map.cpp"

// <algorithm>
using std::make_heap;
using std::pop_heap;
// <iostream>
using std::cout;
using std::endl;
// <string>
using std::string;
// <vector>
using std::vector;

template <typename T>
vector<T> Spectral_Shuffle(vector<vector<T>> &a, string alter_type) {
    // Alter check.
    if (!(alter_type == "Full Alter" || alter_type == "Partial Alter")) {
        cout << "Alter type not valid or not specified. No Alter will be performed." << endl;
    }

    // Alter each group, create a heap of mappings, and find the size of the shuffle.
    vector<mapping<T>> heap;
    unsigned b_size = 0;
    for (unsigned i = 0; i < a.size(); ++i) {
        // Only allow non-empty groups.
        if (a[i].size() > 0) {
            if (alter_type == "Full Alter") {
                Full_Alter(a[i]);
            }
            else if (alter_type == "Partial Alter") {
                Partial_Alter(a[i]);
            }
            heap.emplace_back(&a[i]);
            b_size += a[i].size();
            // Map each item in the group.
            Spectral_Map(heap.back().positions);
        }
    }
    // Restore the heap.
    make_heap(heap.begin(), heap.end());

    // Merge mappings to create a shuffle. The shuffle is filled in descending position order.
    vector<T> b(b_size);
    for (unsigned i = b.size() - 1; i < b.size(); --i) {
        // Store the element with the greatest position between all mappings in the shuffle.
        b[i] = heap.front().elements->at(heap.front().positions.size() - 1);
        // Remove that element's position from its mapping.
        heap.front().positions.pop_back();
        // If every element of the mapping has been added to the shuffle, remove the mapping and restore the heap.
        if (heap.front().positions.size() == 0) {
            pop_heap(heap.begin(), heap.end());
            heap.pop_back();
        }
        // Otherwise, restore the heap without removing any mappings.
        else {
            make_heap(heap.begin(), heap.end());
        }
    }
    // Return the shuffle.
    return b;
}
