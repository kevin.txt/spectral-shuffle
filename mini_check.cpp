// C++ standard library
#include <iostream>
#include <string>
#include <vector>
// Spectral Shuffle
#include "Spectral_Shuffle.cpp"

// <iostream>
using std::cout;
using std::endl;
// <string>
using std::string;
// <vector>
using std::vector;

int main() {
    // Create a vector of vectors of colored squares ■s grouped by color. Make 4 reds, 3 greens, 2 blues, and 1 purple.
    vector<vector<string>> a;
    // #ea8282 red
    a.emplace_back(4, "\033[38;2;234;130;130m■\033[0m ");
    // #9add8f green
    a.emplace_back(3, "\033[38;2;154;221;143m■\033[0m ");
    // #92d2da blue
    a.emplace_back(2, "\033[38;2;146;210;218m■\033[0m ");
    // #d298e4 purple
    a.emplace_back(1, "\033[38;2;210;152;228m■\033[0m ");

    // Print the input.
    cout << "Input" << endl;
    for (unsigned i = 0; i < a.size(); ++i) {
        for (unsigned j = 0; j < a[i].size(); ++j) {
            cout << a[i][j];
        }
    }
    cout << endl;

    // Create a shuffle.
    vector<string> shuffle = Spectral_Shuffle<string>(a, "Full Alter");

    // Print the output.
    cout << "Output" << endl;
    for (unsigned i = 0; i < shuffle.size(); ++i) {
        cout << shuffle[i];
    }
    cout << endl;

    return 0;
}
