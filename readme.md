# Spectral Shuffle
$`O(n^3)`$ time, $`O(n^2)`$ space

The Spectral Shuffle is a perceptually random shuffle based on the eyes of chickens and the properties of random matrix spectra.

This shuffle accepts a set of groups (e.g. songs grouped by artist) and returns a _multi-hyperuniform_ shuffle.

A summary of the shuffle can be found on [Behance](https://www.behance.net/gallery/76329849/Spectral-Shuffle), while the design details and analysis can be found in the [wiki](https://gitlab.com/kevin.txt/spectral-shuffle/wikis/) or on [Medium]().

## Dependencies
* A C++ compiler.
* A C++ standard library.
* ```armadillo```

## Usage

The shuffle is defined as ```Spectral_Shuffle()``` in _Spectral_Shuffle.cpp_. It specifies 2 inputs and 1 output:

### Inputs
* A set of groups.
    * ```vector<vector<typename>> a```
* An Alter type.
    * ```"Full Alter"```
    * ```"Partial Alter"```

### Output
* A _multi-hyperuniform_ shuffle.
    * ```vector<typename> b```

If no valid Alter type is specified, no Alter is performed.

### Full Alter Spectral Shuffle
```c++
b = Spectral_Shuffle(a, "Full Alter");
```

### Partial Alter Spectral Shuffle
```c++
b = Spectral_Shuffle(a, "Partial Alter");
```

### No Alter Spectral Shuffle
```c++
b = Spectral_Shuffle(a, "");
```

## Compiling

A mini check program is included. Run ```make``` in the containing folder to compile it and then type ```./mini_check``` to run it.

### ```gcc``` makefile
The included makefile is written for ```clang``` and ```libc++```. Below is a makefile for ```gcc``` and ```libstdc++```.
```makefile
# Compiler
CXX = gcc

# Flags
CXXFLAGS = -std=c++17 -g -pedantic -Wall -Wextra -Werror

# Libraries
CXXLIBS = -lstdc++ -lm -larmadillo

# Rules
mini_check : mini_check.cpp
	$(CXX) $(CXXFLAGS) $(CXXLIBS) $< -o $@

clean :
	rm -f mini_check
```
