# Compiler
CXX = clang++

# Flags
CXXFLAGS = -std=c++17 -g -pedantic -Wall -Wextra -Werror

# Libraries
CXXLIBS = -stdlib=libc++ -lc++abi -larmadillo

# Rules
mini_check : mini_check.cpp
	$(CXX) $(CXXFLAGS) $(CXXLIBS) $< -o $@

clean :
	rm -f mini_check
