// C++ standard library
#include <cmath>
#include <complex>
#include <random>
#include <vector>
// Armadillo
#include <armadillo>

// <cmath>
using std::sqrt;
// <complex>
using std::conj;
// <random>
using std::normal_distribution;
using std::random_device;
// <vector>
using std::vector;
// <armadillo>
using arma::conv_to;
using arma::cx_double;
using arma::cx_mat;
using arma::eig_sym;
using arma::vec;

void Spectral_Map(vector<double> &positions) {
    // Initialize generator.
    random_device generator;
    // Initialize distributions.
    normal_distribution<double> main_diagonal(0.0, 1.0);
    normal_distribution<double> off_diagonal(0.0, 1.0/sqrt(2.0));

    // Create a Gaussian Unitary Ensemble.
    cx_mat matrix(positions.size(), positions.size());
    for (unsigned i = 0; i < positions.size(); ++i) {
        matrix.at(i, i) = cx_double(main_diagonal(generator), 0.0);
        for (unsigned j = 0; j < i; ++j) {
            matrix.at(i, j) = cx_double(off_diagonal(generator), off_diagonal(generator));
            matrix.at(j, i) = conj(matrix.at(i, j));
        }
    }
    // Normalize the matrix so the spectrum is approximately in [-2,2].
    matrix = matrix/sqrt(positions.size());

    // Perform spectral decomposition.
    vec spectrum;
    eig_sym(spectrum, matrix);
    positions = conv_to<vector<double>>::from(spectrum);
    return;
}
