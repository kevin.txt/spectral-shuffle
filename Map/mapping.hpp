#ifndef MAPPING_H
#define MAPPING_H

// C++ standard library
#include <vector>

// <vector>
using std::vector;

template <typename T>
struct mapping {
    // A tag indicating the set of elements being mapped.
    vector<T> *elements;
    // The positions each element is mapped to.
    vector<double> positions;

    // Parameterized constructor.
    mapping(vector<T> *elements) : elements(elements), positions(elements->size()) {}

    // < operator.
    bool operator < (const mapping<T> &b) const {
        return positions.back() < b.positions.back();
    }
} ;

#endif
